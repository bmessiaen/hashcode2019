from photo import Photo


class Reader(object):

    def __init__(self, filename):
        with open(filename, 'r') as file:
            lines = file.readlines()

        self.N = int(lines[0].strip("\n").split(" ")[0])

        self.data = [[x for x in line.strip("\n").split(" ")] for line in lines[1:]]

        self.listeV = []
        self.listeH = []
        self.tags = {}

        for i in range(len(self.data)):
            photo = Photo(i, self.data[i][0], int(self.data[i][1]),
                          *self.data[i][2:])
            if self.data[i][0] == "H":
                self.listeH.append(photo)

            else:
                self.listeV.append(photo)

            for j in range(2, len(self.data[i])):
                if self.data[i][j] not in self.tags.keys():
                    self.tags[self.data[i][j]] = [photo]
                else:
                    self.tags[self.data[i][j]].append(photo)


if __name__ == '__main__':
    a = Reader("./data/a_example.txt")
