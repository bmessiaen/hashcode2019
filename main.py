from reader import Reader
from slideshow import Slideshow
from writer import Writer

if __name__ == "__main__":
    pass

    reader = Reader("./data/a_example.txt")

    slideshow = Slideshow(reader.N, reader.listeH, reader.listeV, reader.tags)

    slideshow.create_slides()

    Writer.write(slideshow, "a.txt")