class Photo(object):

    def __init__(self, id, ori, nb, *argv):
        self.id = id
        self.ori = ori
        self.tags = set([arg for arg in argv])
        self.nb_tags = nb
