class Writer(object):

    @staticmethod
    def write(slideshow, filename):

        with open("./out/" + filename, 'w') as f:

            # line 1
            f.write(str(len(slideshow.slides)))

            for slide in slideshow.slides:

                # all lines
                f.write('\n')
                f.write(str(slide.photos[0].id))

                if slide.ori == "V":
                    f.write(' ' + str(slide.photos[1].id))


if __name__ == "__main__":

    _object = [1, 2]

    Writer.write(_object, "test.txt")
