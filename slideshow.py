from photo import Photo
from slide import Slide


class Slideshow(object):

    def __init__(self, N, photosH, photosV, tags):

        self.N = N
        self.photosH = photosH
        self.photosV = photosV
        self.tags = tags
        self.slides = []

    def create_slides(self):

        self.slides.append(Slide('H', self.photosH[0]))
        self.slides.append(Slide('H', self.photosH[1]))
        self.slides.append(Slide('V', self.photosV[0], self.photosV[1]))
