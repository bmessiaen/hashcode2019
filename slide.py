class Slide(object):

    def __init__(self, ori, *argv):
        self.ori = ori
        self.photos = [arg for arg in argv]
        self.tags = self.photos[0].tags.union(self.photos[-1].tags)

    def card_inter(self, slide):
        return len(self.tags.intersection(slide.tags))

    def card_diff1(self, slide):
        return len(self.tags.difference(slide.tags))

    def card_diff2(self, slide):
        return len(slide.tags.difference(self.tags))

    def interest_factor(self, slide):
        return min(self.card_inter(slide), self.card_diff1(slide),
                   self.card_diff2(slide))

if __name__ == "__main__":
    from photo import Photo

    photo0 = Photo(0, "H", 3, "cat", "beach", "sun")
    photo3 = Photo(0, "H", 2, "garden", "cat")
    slide1 = Slide("H", photo0)
    slide2 = Slide("H", photo3)
    print(slide1.card_inter(slide2), slide1.card_diff1(slide2),
          slide1.card_diff2(slide2), slide1.interest_factor(slide2))
